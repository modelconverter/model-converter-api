# Model Converter

Reducing Boilerplate Code with model-converter

> More Time for Feature functionality
  Through a simple set of interface and saving 60% of development time 

## Getting Started

Project can be found https://bitbucket.org/modelconverter/model-converter-api.git

### Prerequisites

* Java 1.8
* Maven 3.6.1
* Git
* IDE on your choice

```
I use Intellij as IDE
```

### Installing

``git clone https://bitbucket.org/modelconverter/model-converter-api.git``

``cd model-converter-api``

``mvn clean install``

```
Convert Person to PersonDTO Example.

We just need to implement the update method.
```

```
public class Person2PersonDTOConverter implements Converter<PersonDTO, Person> { 

    /**
     * update dest if orig is not null
     *
     * @param dest should not be null
     * @param orig Original Object
     */
    public void update(PersonDTO dest, Person orig) {

        ...
        dest.setId(orig.getPersonId());
        ...
    }

}


Converter<PersonDTO, Person> converter = new Person2PersonDTOConverter();


// Convert person to PersonDTO
PersonDTO person = converter.convert(person);
 
// Convert to list from Collection
List<PersonDTO> personList = converter.convertList(Collection<Person> persons);
    
// Convert list from Array
List<PersonDTO> personList = converter.convertList(Person[] personArray);

// Convert list from Iterable
List<PersonDTO> personList = converter.convertList(Iterable<Person> personIterable);
    
// Convert set from Collection
Set<PersonDTO> personSet = converter.convertSet(Collection<Person> persons);
    
// Convert set from Array
Set<PersonDTO> personList = converter.convertSet(Person[] persons);
    
// Convert set from Iterator
Set<PersonDTO> personList = converter.convertSet(Iterator<Person> personIterator);

```

```

Or we can just update existing PersonDTO

Updater<PersonDTO, Person> updater = new Person2PersonDTOConverter();


PersonDTO personDTO = ...

updater.update(personDTO, person);
```

```
Sometime there may be a need to interact during converting or updating to pass or share some global context

For this case we can use MetaConverter interface

Meta can be any thing but in this case we use a global Address to person
```
```
public class Person2PersonDTOConverter implements MetaConverter<PersonDTO, Person, Address> { 

    /**
     * update dest if orig is not null and set global address
     *
     * @param dest should not be null
     * @param orig Original Object
     */
    public void update(PersonDTO dest, Person orig, Address meta) {

        ...
        dest.setId(orig.getPersonId());
        dest.setAddress(meta);
        ...
    }

}
```

## Running the tests

```
mvn clean test
```

### And coding style tests

Make sure that the converter are covered by JUnit Test

```
    @Test
    public void convert() {

        PersonDTO dto = sut.convert(model);

        assertNotNull(dto);

        assertEquals(model.getId(), person.getPersinId());
        ...
    }
```

## Deployment

```
mvn clean install
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Mario Tema** - *Initial work* - [Bitbucket](https://bitbucket.org/modelconverter/model-converter-api.git)

See also the list of [contributors](https://bitbucket.org/microtema/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspiration from others converter API's but with small foot print and maximum flexibility

## Technology Stack

* Java 1.8
    * Streams 
    * Lambdas
* Third Party Libraries
    * Commons-BeanUtils (Apache License)
    * Commons-IO (Apache License)
    * Commons-Lang3 (Apache License)
    * Junit (EPL 1.0 License)
* Code-Analyses
    * Sonar
    * Jacoco

