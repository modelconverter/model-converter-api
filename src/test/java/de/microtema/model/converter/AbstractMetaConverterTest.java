package de.microtema.model.converter;

import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.ModelBuilderFactory;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AbstractMetaConverterTest {

    MetaConverter<PersonDTO, Person, String> sut = new MetaConverter<PersonDTO, Person, String>() {
        @Override
        public Class<PersonDTO> getDestinationType() {
            return PersonDTO.class;
        }
    };

    @Model
    Person model;

    String meta = null;

    @Before
    public void setUp() throws Exception {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void convertOnNullWillReturnNull() {
        assertNull(sut.convert(null, meta));
    }

    @Test
    public void convert() {
        PersonDTO dto = sut.convert(model, meta);

        assertNotNull(model);

        assertEquals(dto.getFirstName(), model.getFirstName());
        assertEquals(dto.getLastName(), model.getLastName());
        assertEquals(dto.getAge(), model.getAge());
        assertEquals(dto.getGender(), String.valueOf(model.getGender()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateWillThrowIllegalArgumentException() {

        sut.update(null, model, meta);
    }

    @Test
    public void updateOnNullRight() {

        PersonDTO dto = new PersonDTO();

        sut.update(dto, null, meta);

        assertEquals(dto, new PersonDTO());
    }

    @Test
    public void update() {

        PersonDTO dto = new PersonDTO();

        sut.update(dto, model, meta);

        assertEquals(dto.getFirstName(), model.getFirstName());
        assertEquals(dto.getLastName(), model.getLastName());
        assertEquals(dto.getAge(), model.getAge());
        assertEquals(dto.getGender(), String.valueOf(model.getGender()));
    }

    @Test
    public void convertListOnNull() {
        assertTrue(sut.convertList((List<Person>) null, meta).isEmpty());
    }

    @Test
    public void convertListOnEmpty() {
        assertTrue(sut.convertList(Collections.emptyList(), meta).isEmpty());
    }

    @Test
    public void convertList() {

        Collection<Person> persons = ModelBuilderFactory.list(Person.class);

        List<PersonDTO> dtos = sut.convertList(persons, meta);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertSetOnNull() {
        assertTrue(sut.convertSet((Set<Person>) null, meta).isEmpty());
    }

    @Test
    public void convertSetOnEmpty() {
        assertTrue(sut.convertSet(Collections.emptySet(), meta).isEmpty());
    }

    @Test
    public void convertSet() {
        Collection<Person> persons = ModelBuilderFactory.set(Person.class);

        Set<PersonDTO> dtos = sut.convertSet(persons, meta);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertListFromIterable() {
        Collection<Person> persons = ModelBuilderFactory.list(Person.class);

        List<PersonDTO> dtos = sut.convertList((Iterable<Person>) persons, meta);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertSetFromIterable() {
        Collection<Person> persons = ModelBuilderFactory.set(Person.class);

        Set<PersonDTO> dtos = sut.convertSet((Iterable<Person>) persons, meta);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertListFromArray() {
        Collection<Person> persons = ModelBuilderFactory.list(Person.class);

        List<PersonDTO> dtos = sut.convertList(persons.toArray(new Person[0]), meta);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertSetFromArray() {
        Collection<Person> persons = ModelBuilderFactory.set(Person.class);

        Set<PersonDTO> dtos = sut.convertSet(persons.toArray(new Person[0]), meta);
        assertEquals(persons.size(), dtos.size());
    }

}
