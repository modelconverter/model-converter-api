package de.microtema.model.converter.exception;

import org.junit.Assert;
import org.junit.Test;

public class NoSuchConverterExceptionTest {

    NoSuchConverterException sut;

    @Test
    public void getMessage() {

        sut = new NoSuchConverterException("Foo");
        String expected = "Unable to get Converter by Type: Foo";
        Assert.assertEquals(expected, sut.getMessage());
    }

    @Test(expected = NullPointerException.class)
    public void getMessageWillThroException() {
        sut = new NoSuchConverterException(null);
    }
}
