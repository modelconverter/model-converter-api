package de.microtema.model.converter;

import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.annotation.Models;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class GenericConverterTest {

    @Inject
    GenericConverter sut;

    @Model
    Person model;

    @Models
    List<Person> models;

    @Before
    public void setUp() {

        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void convertWithNull() {

        PersonDTO dto = sut.convert(null, PersonDTO.class);

        assertNull(dto);
    }

    @Test
    public void convert() {

        PersonDTO dto = sut.convert(model, PersonDTO.class);

        assertNotNull(dto);

        Person person = sut.convert(dto, Person.class);

        assertNotNull(dto);

        assertEquals(model, person);
    }

    @Test
    public void convertSameType() {

        Person persons = sut.convert(model, Person.class);

        assertNotNull(persons);

        assertEquals(model, persons);
    }

    @Test
    public void updateWithNull() {

        PersonDTO dto = new PersonDTO();

        sut.update(dto, null);

        assertEquals(dto, new PersonDTO());
    }

    @Test
    public void update() {

        PersonDTO dto = new PersonDTO();

        sut.update(dto, model);

        Person person = sut.convert(dto, Person.class);

        assertNotNull(dto);

        assertEquals(model, person);
    }

    @Test
    public void convertList() {

        List<PersonDTO> persons = sut.convertList(models, PersonDTO.class);

        assertNotNull(persons);

        assertEquals(models, sut.convertList(persons, Person.class));
    }

    @Test
    public void convertSet() {

        Set<PersonDTO> persons = sut.convertSet(models, PersonDTO.class);

        assertNotNull(persons);

        assertEquals(new HashSet<>(models), sut.convertSet(persons, Person.class));
    }

    @Test
    public void matchConvert() {

        boolean matchConverter = sut.matchConverter(new Converter<Person, PersonDTO>() {
            @Override
            public Class<PersonDTO> getSourceType() {
                return PersonDTO.class;
            }

            @Override
            public Class<Person> getDestinationType() {
                return Person.class;
            }
        }, Person.class, PersonDTO.class);

        assertTrue(matchConverter);
    }

    @Test
    public void matchConvertWillReturnFalse() {

        boolean matchConverter = sut.matchConverter(new Converter<Person, PersonDTO>() {
            @Override
            public Class<PersonDTO> getSourceType() {
                return PersonDTO.class;
            }

            @Override
            public Class<Person> getDestinationType() {
                return Person.class;
            }
        }, PersonDTO.class, Person.class);

        assertFalse(matchConverter);
    }

}
