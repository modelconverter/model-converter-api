package de.microtema.model.converter;

import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.annotation.Models;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class GenericMetaConverterTest {

    @Inject
    GenericMetaConverter sut;

    @Model
    Person model;

    @Models
    List<Person> models;

    @Model
    String meta;

    @Before
    public void setUp() {

        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void convertWithNull() {

        PersonDTO dto = sut.convert(null, meta, PersonDTO.class);

        assertNull(dto);
    }

    @Test
    public void convert() {

        PersonDTO dto = sut.convert(model, meta, PersonDTO.class);

        assertNotNull(dto);

        Person person = sut.convert(dto, meta, Person.class);

        assertNotNull(dto);

        assertEquals(model, person);
    }

    @Test
    public void convertSameType() {

        Person persons = sut.convert(model, meta, Person.class);

        assertNotNull(persons);

        assertEquals(model, persons);
    }

    @Test
    public void updateWithNull() {

        PersonDTO dto = new PersonDTO();

        sut.update(dto, null, meta);

        assertEquals(dto, new PersonDTO());
    }

    @Test
    public void update() {

        PersonDTO dto = new PersonDTO();

        sut.update(dto, model, meta);

        Person person = sut.convert(dto, meta, Person.class);

        assertNotNull(dto);

        assertEquals(model, person);
    }

    @Test
    public void convertEmptyList() {

        List<PersonDTO> persons = sut.convertList(Collections.emptyList(), meta, PersonDTO.class);

        assertNotNull(persons);

        assertTrue(persons.isEmpty());
    }

    @Test
    public void convertList() {

        List<PersonDTO> persons = sut.convertList(models, meta, PersonDTO.class);

        assertNotNull(persons);

        assertEquals(models, sut.convertList(persons, meta, Person.class));
    }

    @Test
    public void convertEmptySet() {

        Set<PersonDTO> persons = sut.convertSet(Collections.emptySet(), meta, PersonDTO.class);

        assertNotNull(persons);

        assertTrue(persons.isEmpty());
    }

    @Test
    public void convertSet() {

        Set<PersonDTO> persons = sut.convertSet(models, meta, PersonDTO.class);

        assertNotNull(persons);

        assertEquals(new HashSet<>(models), sut.convertSet(persons, meta, Person.class));
    }

    @Test
    public void matchConvert() {

        boolean matchConverter = sut.matchConverter(new MetaConverter<Person, PersonDTO, String>() {
            @Override
            public Class<PersonDTO> getSourceType() {
                return PersonDTO.class;
            }

            @Override
            public Class<Person> getDestinationType() {
                return Person.class;
            }

            @Override
            public Class<String> getMetaType() {
                return String.class;
            }
        }, Person.class, PersonDTO.class, String.class);

        assertTrue(matchConverter);
    }

    @Test
    public void matchConvertWillReturnFalse() {

        boolean matchConverter = sut.matchConverter(new MetaConverter<Person, PersonDTO, String>() {
            @Override
            public Class<PersonDTO> getSourceType() {
                return PersonDTO.class;
            }

            @Override
            public Class<Person> getDestinationType() {
                return Person.class;
            }

            @Override
            public Class<String> getMetaType() {
                return String.class;
            }
        }, PersonDTO.class, Person.class, String.class);

        assertFalse(matchConverter);
    }
}
