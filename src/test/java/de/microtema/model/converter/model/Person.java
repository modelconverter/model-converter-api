package de.microtema.model.converter.model;

import javax.xml.bind.annotation.XmlElement;
import java.util.Date;
import java.util.Objects;

public class Person {

    @XmlElement(required = true)
    private String firstName;

    @XmlElement(required = true)
    private String lastName;

    @XmlElement(required = true)
    private int age;

    @XmlElement(required = true)
    private GENDER gender;

    private boolean married;

    private Date dob;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public GENDER getGender() {
        return gender;
    }

    public void setGender(GENDER gender) {
        this.gender = gender;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(dob == null ? 0L : dob.getTime(), person.dob == null ? 0L : person.dob.getTime()) &&
                gender == person.gender &&
                married == person.married;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, gender, married, dob);
    }
}

