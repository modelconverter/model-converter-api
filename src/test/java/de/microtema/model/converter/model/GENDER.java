package de.microtema.model.converter.model;

public enum GENDER {

    MALE, FEMALE
}
