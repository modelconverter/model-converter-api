package de.microtema.model.converter.micro;

import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Boolean2StringConverterTest {

    @Inject
    Boolean2StringConverter sut;

    @Model
    Boolean model;

    @Before
    public void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void convertNull() {

        assertNull(sut.convert(null));
    }

    @Test
    public void convert() {

        assertEquals(model.toString(), sut.convert(model));
    }

    @Test
    public void convertMeta() {

        assertEquals(model.toString(), sut.convert(model, null));
    }
}
