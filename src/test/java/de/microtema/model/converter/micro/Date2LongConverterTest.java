package de.microtema.model.converter.micro;

import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Date2LongConverterTest {

    @Inject
    Date2LongConverter sut;

    @Model
    Date model;

    @Before
    public void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void convertNull() {

        assertNull(sut.convert(null));
    }

    @Test
    public void convertMetaWithNull() {

        Long expect = 0L;
        assertEquals(expect, sut.convert(null, Long.class));
    }

    @Test
    public void convert() {

        assertEquals(model.getTime(), sut.convert(model).longValue());
    }

    @Test
    public void convertMeta() {

        assertEquals(model.getTime(), sut.convert(model, null).longValue());
    }
}
