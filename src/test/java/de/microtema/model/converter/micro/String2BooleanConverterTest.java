package de.microtema.model.converter.micro;

import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class String2BooleanConverterTest {

    @Inject
    String2BooleanConverter sut;

    @Model
    Boolean flag;

    String model;

    @Before
    public void setUp() {
        FieldInjectionUtil.injectFields(this);
        model = flag.toString();
    }

    @Test
    public void convertNull() {

        assertNull(sut.convert(null));
    }

    @Test
    public void convert() {

        assertEquals(Boolean.getBoolean(model), sut.convert(model));
    }

    @Test
    public void convertMeta() {

        assertEquals(Boolean.getBoolean(model), sut.convert(model, null));
    }
}
