package de.microtema.model.converter.util;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CollectionUtilTest {

    CollectionUtil sut;

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<CollectionUtil> constructor = CollectionUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void firstOnNull() {

        assertNull(CollectionUtil.first(null));
    }

    @Test
    public void firstOnEmpty() {

        assertNull(CollectionUtil.first(Collections.emptyList()));
    }

    @Test
    public void first() {

        assertEquals("1", CollectionUtil.first(Arrays.asList("1", "2")));
    }

    @Test
    public void lastOnEmpty() {

        assertNull(CollectionUtil.last(Collections.emptyList()));
    }

    @Test
    public void lastOnNull() {

        assertNull(CollectionUtil.last(null));
    }

    @Test
    public void last() {
        assertEquals("2", CollectionUtil.last(Arrays.asList("1", "2")));
    }

    @Test
    public void setAsMap() {
        assertEquals(Collections.singletonMap("1", "1"), CollectionUtil.asMap(new HashSet<>(Arrays.asList("1"))));
    }

    @Test
    public void listAsMap() {
        assertEquals(Collections.singletonMap(0, "One"), CollectionUtil.asMap(Arrays.asList("One")));
    }

    @Test
    public void listAsMapOnNull() {
        assertTrue(CollectionUtil.asMap((List<?>) null).isEmpty());
    }

    @Test
    public void setAsMapOnNull() {
        assertTrue(CollectionUtil.asMap((Set<?>) null).isEmpty());
    }

    @Test
    public void copyList() {
        assertTrue(CollectionUtil.copy((List) null).isEmpty());
    }

    @Test
    public void copyListNotSame() {
        List<String> list = Arrays.asList("Foo");
        List<String> copy = CollectionUtil.copy(list);
        assertNotSame(list, copy);
        assertEquals(list, copy);
    }

    @Test
    public void copySet() {
        assertTrue(CollectionUtil.copy((Set) null).isEmpty());
    }

    @Test
    public void copySetNotSame() {
        Set<String> set = new HashSet<>(Arrays.asList("Foo"));
        Set<String> copy = CollectionUtil.copy(set);
        assertNotSame(set, copy);
        assertEquals(set, copy);
    }

    @Test
    public void trimListToEmpty() {
        assertTrue(CollectionUtil.trimToEmpty((List) null).isEmpty());
    }

    @Test
    public void trimSetToEmpty() {
        assertTrue(CollectionUtil.trimToEmpty((Set) null).isEmpty());
    }

    @Test
    public void asList() {
        assertEquals(Arrays.asList("foo", "bar"), CollectionUtil.asList("foo,bar"));
    }

    @Test
    public void asListWithSeparator() {
        assertEquals(Arrays.asList("foo", "bar"), CollectionUtil.asList("foo,bar", ","));
    }

    @Test
    public void asLongList() {
        assertEquals(Arrays.asList(1L, 3L), CollectionUtil.asLongList("1, 3 "));
    }

    @Test
    public void asLongListSeparator() {
        assertEquals(Arrays.asList(1L, 3L), CollectionUtil.asLongList("1, 3 ", ","));
    }

}

