package de.microtema.model.converter.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

public class ClassUtilTest {

    ClassUtil sut;

    List<String> list; //is needed in test

    Map<String, Integer> map; //is needed in test


    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<ClassUtil> constructor = ClassUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void getGenericType() {
        assertSame(Person.class, ClassUtil.getGenericType(BarFoo.class));
    }

    @Test
    public void getGenericTypeWithIndex() {
        assertSame(Person.class, ClassUtil.getGenericType(BarFoo.class, 0));
    }

    @Test
    public void createInstance() {
        assertNotNull(ClassUtil.createInstance(BarFoo.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createInstanceWillThrowIllegalArgumentException() {
        assertNotNull(ClassUtil.createInstance(ClassUtil.class));
    }

    @Test
    public void getConstructor() {
        assertNotNull(ClassUtil.getConstructor(Person.class));
    }

    @Test(expected = NullPointerException.class)
    public void findClassWillThrowNullPointerException() {
        ClassUtil.findClass(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findClassWillThrowIllegalArgumentException() {
        ClassUtil.findClass(StringUtils.EMPTY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findClassWillThrowIllegalArgumentExceptionOnBlank() {
        ClassUtil.findClass("  ");
    }

    @Test
    public void findClass() {
        assertNull(ClassUtil.findClass("de.foo.bar.Service"));
    }


    @Test
    public void getClassType() {
        assertSame(getClass(), ClassUtil.findClass(getClass().getName()));
    }


    @Test
    public void getGenericTypeFromCollection() throws Exception {

        Type genericType = getClass().getDeclaredField("list").getGenericType();

        Type parameterType = ClassUtil.getGenericType(genericType);

        assertSame(String.class, parameterType);
    }

    @Test
    public void getGenericTypeFromMap() throws Exception {

        Type genericType = getClass().getDeclaredField("map").getGenericType();

        Type parameterType = ClassUtil.getGenericType(genericType, 0);

        assertSame(String.class, parameterType);

        parameterType = ClassUtil.getGenericType(genericType, 1);

        assertSame(Integer.class, parameterType);
    }


    @Test
    public void getClassTest() {

        assertSame(getClass(), ClassUtil.getClass(getClass().getName()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClassTestWillThrowException() {

        ClassUtil.getClass("foo.bar.Foo");
    }


    @Test
    public void findParameterTypes() {
        Class<?>[] parameterTypes = ClassUtil.findParameterTypes(Group.class);

        assertNotNull(parameterTypes);

        assertEquals(1, parameterTypes.length);
        assertSame(String.class, parameterTypes[0]);
    }

    @Test
    public void findParameterTypesWillReturnEmpty() {
        Class<?>[] parameterTypes = ClassUtil.findParameterTypes(Person.class);

        assertNotNull(parameterTypes);

        assertEquals(0, parameterTypes.length);
    }

    @Test
    public void createMapInstance() {

        assertNotNull(ClassUtil.createInstance(Map.class));
    }

    @Test
    public void createListInstance() {

        assertNotNull(ClassUtil.createInstance(List.class));
    }

    @Test
    public void createSetInstance() {

        assertNotNull(ClassUtil.createInstance(Set.class));
    }

    @Test
    public void createDateInstance() {

        assertNotNull(ClassUtil.createInstance(Date.class));
    }

    @Test
    public void createDefaultInstance() {

        Company instance = ClassUtil.createInstance(Company.class);

        assertNotNull(instance);
    }

    @Test
    public void createFallbackInstance() {

        Group instance = ClassUtil.createInstance(Group.class);

        assertNotNull(instance);
    }

    @Test
    public void getGenericTypeOnInterface() {

        Class genericType = ClassUtil.getGenericType(BarFoo.class);

        assertSame(Person.class, genericType);
    }

    public static class Group {
        public Group(String name) {

        }
    }

    public static class Company {

        public Company(String name, Long id, Integer employees) {
        }

        public Company(String name, Long id) {
        }

        public Company() {
        }
    }

    public interface FooBar<T> {
    }

    public static class BarFoo implements FooBar<Person> {
    }

    public static class Person {
    }
}
