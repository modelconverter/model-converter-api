package de.microtema.model.converter;

import de.microtema.model.converter.model.GENDER;
import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.ModelBuilderFactory;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.enums.ModelType;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class AbstractConverterTest {

    Converter<Person, PersonDTO> sut = new Converter<Person, PersonDTO>() {
    };

    @Model(type = ModelType.MAX)
    PersonDTO dto;

    @Before
    public void setUp() throws Exception {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void convertOnNullWillReturnNull() {
        assertNull(sut.convert(null));
    }

    @Test
    public void convert() {

        dto.setGender(ModelBuilderFactory.min(GENDER.class).name());

        Person model = sut.convert(dto);

        assertNotNull(model);

        assertEquals(dto.getFirstName(), model.getFirstName());
        assertEquals(dto.getLastName(), model.getLastName());
        assertEquals(dto.getAge(), model.getAge());
        assertEquals(dto.getGender(), String.valueOf(model.getGender()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateWillThrowNullPointerException() {

        sut.update(null, dto);
    }

    @Test
    public void updateOnNullRight() {

        Person person = new Person();

        sut.update(person, null);

        assertEquals(person, new Person());
    }

    @Test
    public void update() {

        Person model = new Person();

        dto.setGender(ModelBuilderFactory.max(Person.class).getGender().name());
        dto.setMarried(String.valueOf(ModelBuilderFactory.max(Person.class).isMarried()));

        sut.update(model, dto);

        assertEquals(dto.getFirstName(), model.getFirstName());
        assertEquals(dto.getLastName(), model.getLastName());
        assertEquals(dto.getAge(), model.getAge());
        assertEquals(dto.getGender(), model.getGender().name());
        assertEquals(dto.getMarried(), String.valueOf(model.isMarried()));
        assertEquals(dto.getDob(), model.getDob().getTime());
    }

    @Test
    public void convertListOnNull() {
        assertTrue(sut.convertList((List<PersonDTO>) null).isEmpty());
    }

    @Test
    public void convertListOnEmpty() {
        assertTrue(sut.convertList(Collections.emptyList()).isEmpty());
    }

    @Test
    public void convertList() {

        Collection<PersonDTO> dtos = ModelBuilderFactory.list(PersonDTO.class);

        List<Person> persons = sut.convertList(dtos);
        assertEquals(dtos.size(), persons.size());
    }

    @Test
    public void convertSetOnNull() {
        assertTrue(sut.convertSet((Set<PersonDTO>) null).isEmpty());
    }

    @Test
    public void convertSetOnEmpty() {
        assertTrue(sut.convertSet(Collections.emptySet()).isEmpty());
    }

    @Test
    public void convertSet() {
        Collection<PersonDTO> dtos = ModelBuilderFactory.set(PersonDTO.class);

        Set<Person> persons = sut.convertSet(dtos);
        assertEquals(dtos.size(), persons.size());
    }

    @Test
    public void getDestinationType() {
        assertSame(Person.class, sut.getDestinationType());
    }

    @Test
    public void getSourceType() {
        assertSame(PersonDTO.class, sut.getSourceType());
    }

    @Test
    public void convertListFromIterable() {
        Collection<PersonDTO> persons = ModelBuilderFactory.list(PersonDTO.class);

        List<Person> dtos = sut.convertList((Iterable<PersonDTO>) persons);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertSetFromIterable() {
        Collection<PersonDTO> persons = ModelBuilderFactory.set(PersonDTO.class);

        Set<Person> dtos = sut.convertSet((Iterable<PersonDTO>) persons);
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertListFromArray() {
        Collection<PersonDTO> persons = ModelBuilderFactory.list(PersonDTO.class);

        List<Person> dtos = sut.convertList(persons.toArray(new PersonDTO[0]));
        assertEquals(persons.size(), dtos.size());
    }

    @Test
    public void convertSetFromArray() {
        Collection<PersonDTO> persons = ModelBuilderFactory.set(PersonDTO.class);

        Set<Person> dtos = sut.convertSet(persons.toArray(new PersonDTO[0]));
        assertEquals(persons.size(), dtos.size());
    }

}
