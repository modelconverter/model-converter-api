package de.microtema.model.converter;

import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class MetaUpdaterTest {

    @Inject
    DefaultMetaUpdater sut;

    @Model
    PersonDTO model;

    @Model
    Person person;

    @Before
    public void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void update() {

        sut.update(person, model, null);

        assertEquals(person.getLastName(), model.getFirstName());
    }

    @Test
    public void getMetaType() {

        assertSame(Boolean.class, sut.getMetaType());
    }

    public static class DefaultMetaUpdater implements MetaUpdater<Person, PersonDTO, Boolean> {
    }
}
