package de.microtema.model.converter;

import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

public class UpdaterTest {

    @Inject
    DefaultUpdater sut;

    @Model
    PersonDTO model;

    @Model
    Person person;

    @Before
    public void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    public void getDestinationType() {
        assertSame(Person.class, sut.getDestinationType());
    }

    @Test
    public void getSourceType() {

        assertSame(PersonDTO.class, sut.getSourceType());
    }

    @Test
    public void getConverters() {

        assertNotNull(sut.getConverters());
    }

    @Test
    public void update() {

        sut.update(person, model);

        assertEquals(person.getLastName(), model.getFirstName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateWillThrowException() {

        sut.update(null, model);
    }

    public static class DefaultUpdater implements Updater<Person, PersonDTO> {
    }
}
