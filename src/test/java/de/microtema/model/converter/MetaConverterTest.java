package de.microtema.model.converter;

import de.microtema.model.converter.model.Person;
import de.microtema.model.converter.model.PersonDTO;
import de.seven.fate.model.builder.annotation.Inject;
import de.seven.fate.model.builder.annotation.Model;
import de.seven.fate.model.builder.annotation.Models;
import de.seven.fate.model.builder.util.FieldInjectionUtil;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class MetaConverterTest {

    @Inject
    DefaultMetaConverter sut;

    @Models
    List<PersonDTO> models;

    @Model
    Boolean meta;

    @Before
    public void setUp() {
        FieldInjectionUtil.injectFields(this);
        AtomicInteger age = new AtomicInteger(30);
        models.forEach(it -> it.setAge(age.getAndIncrement()));
    }

    @Test
    public void convertEmptyList() {

        List<Person> list = sut.convertList(Collections.emptyList(), meta);

        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void convertNullList() {

        List<Person> list = sut.convertList((List<PersonDTO>) null, meta);

        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void convertList() {

        List<Person> list = sut.convertList(models, meta);

        assertNotNull(list);
        assertEquals(models.size(), list.size());
    }

    @Test
    public void convertListFromEmptyIterable() {

        List<Person> list = sut.convertList(IterableUtils.emptyIterable(), meta);

        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void convertListFromIterable() {

        List<Person> list = sut.convertList((Iterable<PersonDTO>) models, meta);

        assertNotNull(list);
        assertEquals(models.size(), list.size());
    }

    @Test
    public void convertListFromEmptyArray() {

        List<Person> list = sut.convertList(ArrayUtils.toArray(), meta);

        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void convertListFromArray() {

        List<Person> list = sut.convertList(models.toArray(new PersonDTO[0]), meta);

        assertNotNull(list);
        assertEquals(models.size(), list.size());
    }

    @Test
    public void convertEmptySet() {

        Set<Person> set = sut.convertSet(Collections.emptySet(), meta);

        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void convertNullSet() {

        Set<Person> set = sut.convertSet((Set<PersonDTO>) null, meta);

        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void convertSet() {

        Set<Person> set = sut.convertSet(new HashSet<>(models), meta);

        assertNotNull(set);
        assertEquals(models.size(), set.size());
    }

    @Test
    public void convertSetIterable() {

        Set<Person> set = sut.convertSet((Iterable<PersonDTO>) models, meta);

        assertNotNull(set);
        assertEquals(models.size(), set.size());
    }

    @Test
    public void convertEmptyIterable() {

        Set<Person> set = sut.convertSet(IterableUtils.emptyIterable(), meta);

        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void convertSetFromArray() {

        Set<Person> set = sut.convertSet(models.toArray(new PersonDTO[0]), meta);

        assertNotNull(set);
        assertEquals(models.size(), set.size());
    }

    @Test
    public void convertSetFromEmptyArray() {

        Set<Person> set = sut.convertSet(new PersonDTO[0], meta);

        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void convertSetFromNullArray() {

        Set<Person> set = sut.convertSet((PersonDTO[]) null, meta);

        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void getMetaType() {
        assertSame(Boolean.class, sut.getMetaType());
    }

    public static class DefaultMetaConverter implements MetaConverter<Person, PersonDTO, Boolean> {
    }
}
