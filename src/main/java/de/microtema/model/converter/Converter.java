package de.microtema.model.converter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Converter for converting source to destination
 *
 * @param <D> destination Object
 * @param <O> Original Object
 */
public interface Converter<D, O> extends BaseConverter<D, O> {

    /**
     * @param entries may not be null
     * @return converted list or empty list if entries are null or empty
     */
    default List<D> convertList(Collection<O> entries) {

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptyList();
        }

        List<D> list = new ArrayList<>(entries.size());

        for (O entry : entries) {
            list.add(convert(entry));
        }

        return list;
    }

    /**
     * @param iterable may be null or empty
     * @return converted list or empty list if iterable are null or empty
     */
    default List<D> convertList(Iterable<O> iterable) {

        if (IterableUtils.isEmpty(iterable)) {
            return Collections.emptyList();
        }

        return convertList(IterableUtils.toList(iterable));
    }

    /**
     * @param array may be null or empty
     * @return converted list or empty list if array is null or empty
     */
    default List<D> convertList(O[] array) {

        if (ArrayUtils.isEmpty(array)) {
            return Collections.emptyList();
        }

        return convertList(Arrays.asList(array));
    }

    /**
     * @param entries may not be null
     * @return converted set or empty set if entries are null or empty
     */
    default Set<D> convertSet(Collection<O> entries) {

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptySet();
        }

        Set<D> set = new HashSet<>(entries.size());

        for (O entry : entries) {
            set.add(convert(entry));
        }

        return set;
    }

    /**
     * @param iterable may be null or empty
     * @return converted set or empty set if iterable are null or empty
     */
    default Set<D> convertSet(Iterable<O> iterable) {

        if (IterableUtils.isEmpty(iterable)) {
            return Collections.emptySet();
        }

        return convertSet(IterableUtils.toList(iterable));
    }

    /**
     * @param array may be null or empty
     * @return converted set or empty set if iterable are null or empty
     */
    default Set<D> convertSet(O[] array) {

        if (ArrayUtils.isEmpty(array)) {
            return Collections.emptySet();
        }

        return convertSet(Arrays.asList(array));
    }

}
