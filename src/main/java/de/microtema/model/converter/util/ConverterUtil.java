package de.microtema.model.converter.util;

import de.microtema.model.converter.MetaConverter;
import de.microtema.model.converter.micro.Boolean2StringConverter;
import de.microtema.model.converter.micro.Date2LongConverter;
import de.microtema.model.converter.micro.Enum2StringConverter;
import de.microtema.model.converter.micro.Long2DateConverter;
import de.microtema.model.converter.micro.String2BooleanConverter;
import de.microtema.model.converter.micro.String2EnumConverter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.collections4.keyvalue.AbstractKeyValue;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Converter Util class
 */
public final class ConverterUtil {

    private static final Map<Class, Set<Property>> CACHE = new HashMap<>();
    private static final Set<MetaConverter> DEFAULT_CONVERTERS = new HashSet<>();

    private ConverterUtil() {
        throw new IllegalStateException("Should not create new instance");
    }

    /**
     * @param dest       may not be null
     * @param orig       may be null
     * @param converters may not be null
     * @throws IllegalAccessException    if the caller does not have
     *                                   access to the property accessor method
     * @throws IllegalArgumentException  if <code>bean</code> or
     *                                   <code>name</code> is null
     * @throws IllegalArgumentException  if the property name is
     *                                   nested or indexed
     * @throws InvocationTargetException if the property accessor method
     *                                   throws an exception
     * @throws NoSuchMethodException     if an accessor method for this
     *                                   property cannot be found
     * @see PropertyUtilsBean#setSimpleProperty
     */
    public static void update(Object dest, Object orig, Set<MetaConverter> converters) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        notNull(dest);
        notNull(converters);

        if (orig == null) {
            return;
        }

        Set<Property> origProperties = getProperties(orig.getClass());
        Set<Property> destProperties = getProperties(dest.getClass());

        Set<String> validProperties = destProperties.stream().map(AbstractKeyValue::getKey).collect(Collectors.toSet());

        origProperties.removeIf(it -> !validProperties.contains(it.getKey()));

        for (Property property : origProperties) {

            String propertyName = property.getKey();
            Object propertyValue = getPropertyValue(orig, converters, property, destProperties);

            PropertyUtils.setSimpleProperty(dest, propertyName, propertyValue);
        }
    }

    /**
     * Return lazy default converters
     *
     * @return Set
     */
    public static Set<MetaConverter> getDefaultConverters() {

        if (DEFAULT_CONVERTERS.isEmpty()) {

            DEFAULT_CONVERTERS.add(ClassUtil.createInstance(Boolean2StringConverter.class));
            DEFAULT_CONVERTERS.add(ClassUtil.createInstance(String2BooleanConverter.class));

            DEFAULT_CONVERTERS.add(ClassUtil.createInstance(Date2LongConverter.class));
            DEFAULT_CONVERTERS.add(ClassUtil.createInstance(Long2DateConverter.class));

            DEFAULT_CONVERTERS.add(ClassUtil.createInstance(Enum2StringConverter.class));
            DEFAULT_CONVERTERS.add(ClassUtil.createInstance(String2EnumConverter.class));
        }

        return DEFAULT_CONVERTERS;
    }

    /**
     * @param orig           may not be null
     * @param converters     may not be null
     * @param property       may not be null
     * @param destProperties may not be null
     * @return Object
     * @throws IllegalAccessException    if the caller does not have
     *                                   access to the property accessor method
     * @throws IllegalArgumentException  if <code>bean</code> or
     *                                   <code>name</code> is null
     * @throws IllegalArgumentException  if the property name
     *                                   is nested or indexed
     * @throws InvocationTargetException if the property accessor method
     *                                   throws an exception
     * @throws NoSuchMethodException     if an accessor method for this
     *                                   property cannot be found
     */
    private static Object getPropertyValue(Object orig, Set<MetaConverter> converters, Property property, Set<Property> destProperties) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        assert orig != null;
        assert property != null;
        assert converters != null;
        assert destProperties != null;

        String propertyName = property.getKey();

        Class propertyType = property.getValue();

        Object propertyValue = PropertyUtils.getSimpleProperty(orig, propertyName);

        Property destProperty = destProperties.stream().filter(it -> Objects.equals(it.getKey(), propertyName)).findFirst().get();

        Optional<MetaConverter> converter = getMetaConverter(converters, propertyType, destProperty.getValue());

        if (converter.isPresent()) {

            return getPropertyValue(converter.get(), propertyValue, destProperty.getValue());
        }

        return propertyValue;
    }

    /**
     * get Property Value if meta converter is not null else property value
     *
     * @param metaConverter    may be null
     * @param propertyValue    may be null
     * @param destPropertyType may be null
     * @return Object
     */
    private static Object getPropertyValue(MetaConverter metaConverter, Object propertyValue, Class destPropertyType) {

        if (metaConverter != null) {

            return metaConverter.convert(propertyValue, destPropertyType);
        }

        return propertyValue;
    }

    /**
     * @param converters       may not be null
     * @param propertyType     may not be null
     * @param destPropertyType may not be null
     * @return Optional<MetaConverter>
     */
    private static Optional<MetaConverter> getMetaConverter(Set<MetaConverter> converters, Class propertyType, Class destPropertyType) {
        assert converters != null;
        assert propertyType != null;

        return converters.stream()
                .filter(it -> fixType(propertyType).isAssignableFrom(it.getSourceType()))
                .filter(it -> fixType(destPropertyType).isAssignableFrom(it.getDestinationType())).findAny();
    }

    /**
     * fix Type to boxed type
     *
     * @param type may not be null
     * @return Class
     */
    private static Class fixType(Class<?> type) {
        assert type != null;

        if (type.isEnum()) {
            return Enum.class;
        }

        if (type == boolean.class) {

            return Boolean.class;
        }

        if (type == int.class) {

            return Integer.class;
        }

        if (type == long.class) {

            return Long.class;
        }

        return type;
    }

    private static Set<Property> getProperties(Class beanType) {

        if (CACHE.containsKey(beanType)) {

            return CACHE.get(beanType);
        }

        Set<Property> properties = getPropertiesImpl(beanType);

        CACHE.put(beanType, properties);

        return properties;
    }

    private static Set<Property> getPropertiesImpl(Class beanType) {

        PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(beanType);

        return Stream.of(propertyDescriptors)
                .filter(it -> !"class".equals(it.getName()))
                .map(it -> new Property(it.getName(), it.getPropertyType()) {
                }).collect(Collectors.toSet());
    }

    static class Property extends AbstractKeyValue<String, Class<?>> {

        Property(String key, Class<?> value) {
            super(key, value);
        }
    }
}
