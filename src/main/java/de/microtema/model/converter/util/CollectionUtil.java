package de.microtema.model.converter.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Collection Util
 */
public final class CollectionUtil {

    private static final String SEPARATOR = ",";

    private CollectionUtil() {
        throw new UnsupportedOperationException(getClass().getName() + " should not be called with new!");
    }

    /**
     * Return first entry of collection
     *
     * @param collection may be null
     * @param <E>        required type
     * @return first entry of collection or throw IllegalStateException
     */
    public static <E> E first(Collection<E> collection) {

        if (CollectionUtils.isEmpty(collection)) {
            return null;
        }

        Iterator<E> iterator = collection.iterator();

        if (iterator.hasNext()) {
            return iterator.next();
        }

        throw new IllegalStateException("Should not happen");
    }

    /**
     * Return last entry of collection
     *
     * @param collection may be null
     * @param <E>        required type
     * @return last entry of collection or throw IllegalStateException
     */
    public static <E> E last(List<E> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            return null;
        }

        return collection.get(collection.size() - 1);
    }

    /**
     * Map set as Map
     *
     * @param collections may be null
     * @param <E>         required type
     * @return Map of required type
     */
    public static <E> Map<E, E> asMap(Set<E> collections) {
        if (CollectionUtils.isEmpty(collections)) {
            return Collections.emptyMap();
        }

        return collections.stream().collect(Collectors.toMap(it -> (E) it, it -> (E) it));
    }

    /**
     * Map set as Map
     *
     * @param collections may be null
     * @param <E>         required type
     * @return Map of required type
     */
    public static <E> Map<Integer, E> asMap(List<E> collections) {
        if (CollectionUtils.isEmpty(collections)) {
            return Collections.emptyMap();
        }

        Map<Integer, E> map = new HashMap<>();

        for (int index = 0; index < collections.size(); index++) {
            map.put(index, collections.get(index));
        }

        return map;
    }

    /**
     * Create new Instance of given list
     *
     * @param list to be copy
     * @param <E>  required type
     * @return List of required type
     */
    public static <E> List<E> copy(List<E> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        return new ArrayList<>(list);
    }

    /**
     * Create new Instance of given set
     *
     * @param set to be copy
     * @param <E> required type
     * @return Set of required type
     */
    public static <E> Set<E> copy(Set<E> set) {
        if (CollectionUtils.isEmpty(set)) {
            return Collections.emptySet();
        }

        return new HashSet<>(set);
    }

    /**
     * Trim list to empty if null or return the given one
     *
     * @param list to be trimmed
     * @param <E>  required type
     * @return List or required type
     */
    public static <E> List<E> trimToEmpty(List<E> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        return list;
    }

    /**
     * Trim set to empty if null or return the given one
     *
     * @param set to be trimmed
     * @param <E> required type
     * @return Set or required type
     */
    public static <E> Set<E> trimToEmpty(Set<E> set) {
        if (CollectionUtils.isEmpty(set)) {
            return Collections.emptySet();
        }

        return set;
    }

    /**
     * Splits this string around comma ',' of the given str
     *
     * @param str to be split
     * @return List of String
     */
    public static List<String> asList(String str) {

        return asList(str, SEPARATOR);
    }

    /**
     * Splits this string around separator of the given str
     *
     * @param str       to be split
     * @param separator for str
     * @return List of String
     */
    public static List<String> asList(String str, String separator) {
        if (StringUtils.isBlank(str)) {
            return Collections.emptyList();
        }

        return Stream.of(str.split(separator)).map(StringUtils::trimToNull).filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }


    /**
     * Splits this string around separator of the given str and parse to Long
     *
     * @param str       to be split
     * @param separator for str
     * @return List of Long
     */
    public static List<Long> asLongList(String str, String separator) {
        if (StringUtils.isBlank(str)) {
            return Collections.emptyList();
        }

        return asList(str, separator).stream().map(Long::parseLong).collect(Collectors.toList());
    }

    /**
     * Splits this string around comma ',' of the given str and parse to Long
     *
     * @param str to be split
     * @return List of Long
     */
    public static List<Long> asLongList(String str) {
        if (StringUtils.isBlank(str)) {
            return Collections.emptyList();
        }

        return asLongList(str, SEPARATOR);
    }
}
