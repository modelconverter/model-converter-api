package de.microtema.model.converter.micro;

import de.microtema.model.converter.MetaConverter;

/**
 * Convert Boolean to String
 */
public class Boolean2StringConverter implements MetaConverter<String, Boolean, Class<String>> {

    /**
     * Convert Boolean to String if defined else null
     *
     * @param orig should not be null
     * @param meta share some meta data
     * @return String
     */
    @Override
    public String convert(Boolean orig, Class<String> meta) {

        if (orig == null) {

            return null;
        }

        return orig.toString();
    }

    /**
     * Convert Boolean to String if defined else null
     *
     * @param orig should not be null
     * @return String
     */
    @Override
    public String convert(Boolean orig) {

        if (orig == null) {

            return null;
        }

        return convert(orig, null);
    }
}
