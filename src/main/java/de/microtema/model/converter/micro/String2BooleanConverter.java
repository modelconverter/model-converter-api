package de.microtema.model.converter.micro;

import de.microtema.model.converter.MetaConverter;

public class String2BooleanConverter implements MetaConverter<Boolean, String, Class<Boolean>> {

    @Override
    public Boolean convert(String orig, Class<Boolean> meta) {

        if (orig == null) {

            return meta == null ? null : Boolean.FALSE;
        }

        return Boolean.getBoolean(orig);
    }

    @Override
    public Boolean convert(String orig) {

        return convert(orig, null);
    }
}
