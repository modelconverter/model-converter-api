package de.microtema.model.converter.micro;

import de.microtema.model.converter.MetaConverter;

public class Enum2StringConverter implements MetaConverter<String, Enum, Class<String>> {

    @Override
    public String convert(Enum orig, Class<String> meta) {

        if (orig == null) {

            return null;
        }

        return String.valueOf(orig);
    }

    @Override
    public String convert(Enum orig) {

        return convert(orig, null);
    }
}
