package de.microtema.model.converter.micro;

import de.microtema.model.converter.MetaConverter;

import java.util.Date;

public class Date2LongConverter implements MetaConverter<Long, Date, Class<Long>> {

    @Override
    public Long convert(Date orig, Class<Long> meta) {

        if (orig == null) {

            return meta == null ? null : 0L;
        }

        return orig.getTime();
    }

    @Override
    public Long convert(Date orig) {

        return convert(orig, null);
    }
}
