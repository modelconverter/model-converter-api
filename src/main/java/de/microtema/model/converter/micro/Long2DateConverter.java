package de.microtema.model.converter.micro;

import de.microtema.model.converter.MetaConverter;

import java.util.Date;

public class Long2DateConverter implements MetaConverter<Date, Long, Class<Date>> {

    @Override
    public Date convert(Long orig, Class<Date> meta) {

        if (orig == null) {

            return null;
        }

        return new Date(orig);
    }

    @Override
    public Date convert(Long orig) {

        return convert(orig, null);
    }
}
