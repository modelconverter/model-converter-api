package de.microtema.model.converter.micro;

import de.microtema.model.converter.MetaConverter;

public class String2EnumConverter implements MetaConverter<Enum, String, Class<Enum>> {

    @SuppressWarnings("unchecked")
    @Override
    public Enum convert(String orig, Class<Enum> meta) {

        if (orig == null) {

            return null;
        }

        return Enum.valueOf(meta, orig);
    }
}
