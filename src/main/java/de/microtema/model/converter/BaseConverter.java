package de.microtema.model.converter;

import de.microtema.model.converter.util.ClassUtil;

/**
 * Converter for converting source to destination
 *
 * @param <D> destination Object
 * @param <O> Original Object
 */
public interface BaseConverter<D, O> extends Updater<D, O> {

    /**
     * Create new instance from destionation and update with original
     *
     * @param orig object
     * @return converted Object or null if orig is null
     */
    default D convert(O orig) {

        if (orig == null) {
            return null;
        }

        D instance = createInstance();

        update(instance, orig);

        return instance;
    }

    /**
     * Create new instance for destination
     *
     * @return Object
     */
    default D createInstance() {

        Class<D> genericType = getDestinationType();

        return ClassUtil.createInstance(genericType);
    }

}
