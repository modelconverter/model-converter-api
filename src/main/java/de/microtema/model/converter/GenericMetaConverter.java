package de.microtema.model.converter;


import de.microtema.model.converter.util.CollectionUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;


/**
 * Default Generic Meta Converter
 * Convert bean to another type by given converters or create on on the fly and store it
 * NOTE: Use this generic converter as last option and prefer create a dedicated one
 */
@SuppressWarnings("unchecked")
public class GenericMetaConverter {

    private final Set<MetaConverter> converters;

    public GenericMetaConverter(Set<MetaConverter> converters) {
        this.converters = converters;
    }

    /**
     * @param orig may be null
     * @param dest may not be null
     * @param meta may not be null
     * @param <D>  dest type
     * @param <M>  meta type
     * @return new Instance from orig or null
     */
    public <D, M> D convert(Object orig, M meta, Class<D> dest) {
        if (orig == null) {
            return null;
        }

        MetaConverter converter = findConverter(dest, orig.getClass(), meta.getClass());

        return (D) converter.convert(orig, meta);
    }

    /**
     * @param orig may be null
     * @param dest may not be null
     * @param meta may not be null
     * @param <D>  dest type
     */
    public <D> void update(D dest, Object orig, Object meta) {
        Validate.notNull(dest);

        if (orig == null) {
            return;
        }

        MetaConverter converter = findConverter(dest.getClass(), orig.getClass(), meta.getClass());

        converter.update(dest, orig, meta);
    }

    /**
     * @param entries may be null or empty
     * @param type    may be null
     * @param meta    may not be null
     * @param <D>     dest type
     * @param <O>     orig type
     * @return List of converted entries or Empty List but never null
     */
    public <D, O> List<D> convertList(Collection<O> entries, Object meta, Class<D> type) {
        Validate.notNull(type);

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptyList();
        }

        O orig = CollectionUtil.first(entries);

        MetaConverter converter = findConverter(type, orig.getClass(), meta.getClass());

        return (List<D>) converter.convertList(entries, meta);
    }

    /**
     * @param entries may be null or empty
     * @param type    may be null
     * @param meta    may not be null
     * @param <D>     dest type
     * @param <O>     orig type
     * @return Set of converted entries or Empty List but never null
     */
    public <D, O> Set<D> convertSet(Collection<O> entries, Object meta, Class<D> type) {
        Validate.notNull(type);

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptySet();
        }

        O orig = CollectionUtil.first(entries);

        MetaConverter converter = findConverter(type, orig.getClass(), meta.getClass());

        return (Set<D>) converter.convertSet(entries, meta);
    }


    private MetaConverter findConverter(Class destinationType, Class sourceType, Class metaType) {
        assert destinationType != null;
        assert sourceType != null;

        return converters.stream().filter(it -> matchConverter(it, destinationType, sourceType, metaType)).findAny().orElseGet(() -> createAndStore(destinationType, sourceType, metaType));
    }

    boolean matchConverter(MetaConverter converter, Class destinationType, Class sourceType, Class metaType) {
        assert converter != null;
        assert destinationType != null;
        assert sourceType != null;
        assert metaType != null;

        Class converterSourceType = converter.getSourceType();
        Class converterDestinationType = converter.getDestinationType();
        Class converterMetaType = converter.getMetaType();

        boolean validTypes = converterSourceType != null && converterDestinationType != null && converterMetaType != null;

        return validTypes && sourceType.isAssignableFrom(converterSourceType) && destinationType.isAssignableFrom(converterDestinationType) && metaType.isAssignableFrom(converterMetaType);
    }

    private <D, O, M> MetaConverter<D, O, M> createAndStore(Class<D> destinationType, Class<O> sourceType, Class<M> meta) {

        MetaConverter<D, O, M> converter = new MetaConverter<D, O, M>() {

            @Override
            public Class<O> getSourceType() {

                return sourceType;
            }

            @Override
            public Class<D> getDestinationType() {

                return destinationType;
            }

            @Override
            public Class<M> getMetaType() {

                return meta;
            }
        };

        converters.add(converter);

        return converter;
    }

}
