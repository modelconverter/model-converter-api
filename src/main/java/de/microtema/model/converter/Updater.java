package de.microtema.model.converter;

import de.microtema.model.converter.util.ClassUtil;
import de.microtema.model.converter.util.ConverterUtil;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Updater for updating destination from source
 *
 * @param <D> destination Object
 * @param <O> Original Object
 */
public interface Updater<D, O> {

    /**
     * update dest if orig is not null
     *
     * @param dest should not be null
     * @param orig Original Object
     */
    default void update(D dest, O orig) {

        try {
            ConverterUtil.update(dest, orig, getConverters());
        } catch (Exception e) {
            Logger.getLogger(Updater.class.getCanonicalName()).log(Level.WARNING, "Unable to copy properties!", e);

            throw new IllegalArgumentException(e);
        }
    }

    /**
     * @return destination class type
     */
    default Class<D> getDestinationType() {

        return ClassUtil.getGenericType(getClass());
    }

    /**
     * @return source/orig class type
     */
    default Class<O> getSourceType() {

        return ClassUtil.getGenericType(getClass(), 1);
    }

    default Set<MetaConverter> getConverters() {

        return ConverterUtil.getDefaultConverters();
    }
}
