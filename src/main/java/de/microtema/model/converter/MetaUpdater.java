package de.microtema.model.converter;

import de.microtema.model.converter.util.ClassUtil;

/**
 * MetaUpdater extends Updater with meta object
 *
 * @param <D> Destination Object
 * @param <O> Original Object
 * @param <M> Meta object
 */
public interface MetaUpdater<D, O, M> extends Updater<D, O> {

    /**
     * update dest if orig is not null
     *
     * @param dest should not be null
     * @param meta share some meta data
     * @param orig ma not be null
     */
    default void update(D dest, O orig, M meta) {

        update(dest, orig);
    }

    /**
     * @return meta class type
     */
    default Class<M> getMetaType() {

        return ClassUtil.getGenericType(getClass(), 2);
    }
}
