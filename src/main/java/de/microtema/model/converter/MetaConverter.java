package de.microtema.model.converter;

import de.microtema.model.converter.util.ClassUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

/**
 * Meta Converter sharing some common data
 *
 * @param <D> Destination Object
 * @param <O> Original Object
 * @param <M> Meta object
 */
public interface MetaConverter<D, O, M> extends BaseMetaConverter<D, O, M> {

    /**
     * @param entries may be null or empty
     * @param meta    share some meta data
     * @return converted list or empty list if entries are null or empty
     */
    default List<D> convertList(Collection<O> entries, M meta) {

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptyList();
        }

        List<D> list = new ArrayList<>(entries.size());

        for (O entry : entries) {
            list.add(convert(entry, meta));
        }

        return list;
    }

    /**
     * @param iterable may be null or empty
     * @param meta     share some meta data
     * @return converted list or empty list if entries are null or empty
     */
    default List<D> convertList(Iterable<O> iterable, M meta) {

        if (IterableUtils.isEmpty(iterable)) {
            return Collections.emptyList();
        }

        return convertList(IterableUtils.toList(iterable), meta);
    }

    /**
     * @param array may be null or empty
     * @param meta  share some meta data
     * @return converted list or empty list if entries are null or empty
     */
    default List<D> convertList(O[] array, M meta) {

        if (ArrayUtils.isEmpty(array)) {
            return Collections.emptyList();
        }

        return convertList(Arrays.asList(array), meta);
    }

    /**
     * @param entries may be null or empty
     * @param meta    share some meta data
     * @return converted set or empty set if entries are null or empty
     */
    default Set<D> convertSet(Collection<O> entries, M meta) {

        if (isEmpty(entries)) {
            return Collections.emptySet();
        }

        Set<D> set = new HashSet<>(entries.size());

        for (O entry : entries) {
            set.add(convert(entry, meta));
        }

        return set;
    }

    /**
     * @param iterable may be null or empty
     * @param meta     share some meta data
     * @return converted list or empty list if entries are null or empty
     */
    default Set<D> convertSet(Iterable<O> iterable, M meta) {

        if (IterableUtils.isEmpty(iterable)) {
            return Collections.emptySet();
        }

        return convertSet(IterableUtils.toList(iterable), meta);
    }

    /**
     * @param array may be null or empty
     * @param meta  share some meta data
     * @return converted list or empty list if entries are null or empty
     */
    default Set<D> convertSet(O[] array, M meta) {

        if (ArrayUtils.isEmpty(array)) {
            return Collections.emptySet();
        }

        return convertSet(Arrays.asList(array), meta);
    }

    /**
     * @return meta class type
     */
    @Override
    default Class<M> getMetaType() {

        return ClassUtil.getGenericType(getClass(), 2);
    }
}
