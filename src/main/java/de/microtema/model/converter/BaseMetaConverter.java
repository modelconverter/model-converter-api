package de.microtema.model.converter;

/**
 * Meta Converter sharing some common data
 *
 * @param <D> destination generic type
 * @param <O> original generic type
 * @param <M> mety generic type
 */
public interface BaseMetaConverter<D, O, M> extends Converter<D, O>, MetaUpdater<D, O, M> {

    /**
     * @param orig should not be null
     * @param meta share some meta data
     * @return converted Object or null if orig is null
     */
    default D convert(O orig, M meta) {

        if (orig == null) {
            return null;
        }

        D instance = createInstance();

        update(instance, orig, meta);

        return instance;
    }


}
