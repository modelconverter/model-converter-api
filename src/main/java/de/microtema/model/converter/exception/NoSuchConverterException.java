package de.microtema.model.converter.exception;

import java.util.NoSuchElementException;

import static org.apache.commons.lang3.Validate.notNull;

public class NoSuchConverterException extends NoSuchElementException {

    public NoSuchConverterException(String message) {
        super("Unable to get Converter by Type: " + notNull(message));
    }
}
