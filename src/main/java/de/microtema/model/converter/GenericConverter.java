package de.microtema.model.converter;

import de.microtema.model.converter.util.CollectionUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;


/**
 * Default Generic Converter
 * Convert bean to another type by given converters or create on on the fly and store it
 * NOTE: Use this generic converter as last option and prefer create a dedicated one
 */
@SuppressWarnings("unchecked")
public class GenericConverter {

    private final Set<Converter> converters;

    public GenericConverter(Set<Converter> converters) {
        this.converters = converters;
    }

    /**
     * @param orig may be null
     * @param dest may not be null
     * @param <D>  dest type
     * @return new Instance from orig or null
     */
    public <D> D convert(Object orig, Class<D> dest) {
        if (orig == null) {
            return null;
        }

        Converter converter = findConverter(dest, orig.getClass());

        return (D) converter.convert(orig);
    }


    /**
     * @param dest may not be null
     * @param orig may be null
     * @param <D>  dest type
     */
    public <D> void update(D dest, Object orig) {
        Validate.notNull(dest);

        if (orig == null) {
            return;
        }

        Converter converter = findConverter(dest.getClass(), orig.getClass());

        converter.update(dest, orig);
    }


    /**
     * @param entries may be null or empty
     * @param type    may not be null
     * @param <D>     dest type
     * @param <O>     orig type
     * @return List of converted entries or Empty List but never null
     */
    public <D, O> List<D> convertList(Collection<O> entries, Class<D> type) {
        Validate.notNull(type);

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptyList();
        }

        O orig = CollectionUtil.first(entries);

        Converter converter = findConverter(type, orig.getClass());

        return (List<D>) converter.convertList(entries);
    }

    /**
     * @param iterable may be null or empty
     * @param type     may not be null
     * @param <D>      dest type
     * @param <O>      orig type
     * @return List of converted iterable or Empty List but never null
     */
    public <D, O> List<D> convertList(Iterable<O> iterable, Class<D> type) {
        Validate.notNull(type);

        if (IterableUtils.isEmpty(iterable)) {
            return Collections.emptyList();
        }

        return convertList(IterableUtils.toList(iterable), type);
    }

    /**
     * @param entries may be null or empty
     * @param type    may not be null
     * @param <D>     dest type
     * @param <O>     orig type
     * @return Set of converted entries or Empty List but never null
     */
    public <D, O> Set<D> convertSet(Collection<O> entries, Class<D> type) {
        Validate.notNull(type);

        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptySet();
        }

        O orig = CollectionUtil.first(entries);

        Converter converter = findConverter(type, orig.getClass());

        return (Set<D>) converter.convertSet(entries);
    }

    /**
     * @param iterable may be null or empty
     * @param type     may not be null
     * @param <D>      dest type
     * @param <O>      orig type
     * @return List of converted iterable or Empty List but never null
     */
    public <D, O> Set<D> convertSet(Iterable<O> iterable, Class<D> type) {
        Validate.notNull(type);

        if (IterableUtils.isEmpty(iterable)) {
            return Collections.emptySet();
        }

        return convertSet(IterableUtils.toList(iterable), type);
    }

    private Converter findConverter(Class destinationType, Class sourceType) {
        assert destinationType != null;
        assert sourceType != null;

        return converters.stream().filter(it -> matchConverter(it, destinationType, sourceType))
                .findAny()
                .orElseGet(() -> createAndStore(destinationType, sourceType));
    }

    boolean matchConverter(Converter converter, Class destinationType, Class sourceType) {
        assert converter != null;
        assert destinationType != null;
        assert sourceType != null;

        Class converterSourceType = converter.getSourceType();
        Class converterDestinationType = converter.getDestinationType();

        boolean validTypes = converterSourceType != null && converterDestinationType != null;

        return validTypes && sourceType.isAssignableFrom(converterSourceType) && destinationType.isAssignableFrom(converterDestinationType);
    }

    private <D, O> Converter<D, O> createAndStore(Class<D> destinationType, Class<O> sourceType) {
        assert destinationType != null;
        assert sourceType != null;

        Converter<D, O> converter = new Converter<D, O>() {

            @Override
            public Class<O> getSourceType() {

                return sourceType;
            }

            @Override
            public Class<D> getDestinationType() {

                return destinationType;
            }
        };

        converters.add(converter);

        return converter;
    }
}
